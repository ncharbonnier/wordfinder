﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordFinder
{

    public class WordFinder
    {

        private List<string> _dictionary;

        public WordFinder(IEnumerable<string> dictionary)
        {
            _dictionary = dictionary.ToList();
        }
        
        /// <summary>
        /// Returns the set of words in _dictionary found in src
        /// </summary>
        /// <param name="src">Array of strings of the same length representing the words matrix</param>
        /// <returns></returns>
        public IList<string> Find(IEnumerable<string> src)
        {
            // List of strings found in src
            List<string> result = new List<string>();

            // If src is empty we return an empty list
            if (src == null || src.Count() == 0)
            {
                return result;
            }

            
            List<string> verticalWords = new List<string>();
            int columnsLength = src.ElementAt(0).Length;
            // Generate a list of vertical words
            for (int i = 0; i < columnsLength; i++)
            {
                // Build the horizontal word with the characters in the same index
                StringBuilder sb = new StringBuilder();
                foreach (string item in src)
                {
                    sb.Append(item[i]);
                }
                // Add the word to the list
                verticalWords.Add(sb.ToString());
            }

            //If the word is not currently in the result, we check if it is contained within the vertical or horizontal strings
            foreach (string word in _dictionary)
            {
                if (!result.Contains(word)
                    && (src.Count(w => w.IndexOf(word) >= 0) > 0 
                    || verticalWords.Count(w => w.IndexOf(word) >= 0) > 0))
                {
                    result.Add(word);
                }
            }

            return result;
        }
    }

}
