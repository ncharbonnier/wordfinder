﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordFinder.Tests
{
    [TestClass()]
    public class WordFinderTests
    {

        [TestMethod()]
        public void FindTest()
        {
            var dictionary = new string[] { "chill", "wind", "snow", "cold" };
            var src = new string[] { "abcdc", "fgwio", "chill", "pqnsd", "uvdxy" };
            var result = new WordFinder(dictionary).Find(src);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 3);
            Assert.IsTrue(result.Count(r => r == "chill" || r == "cold" || r == "wind") == 3);

        }

        [TestMethod()]
        public void FindNoneTest()
        {
            var dictionary = new string[] { "chilly", "window", "snow", "coldplay" };
            var src = new string[] { "abcdc", "fgwio", "chill", "pqnsd", "uvdxy" };
            var result = new WordFinder(dictionary).Find(src);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 0);

        }

        [TestMethod()]
        public void FindEmptyDictionaryTest()
        {
            var dictionary = new string[] { };
            var src = new string[] { "abcdc", "fgwio", "chill", "pqnsd", "uvdxy" };
            var result = new WordFinder(dictionary).Find(src);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 0);

        }
    }
}